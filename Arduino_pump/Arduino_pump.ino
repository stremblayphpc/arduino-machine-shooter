#define MAX_MILLIS_TO_WAIT 1000
#define PUMP_START 500

int pins[13-2] = {};
int pinCount = 3;//Number of Pump in function max(11x)
int sensorPin = A0;// select the input pin for the potentiometer

int instruction[2];

String outputSerial = "";
int outputFunction = 0;
int incomingData;// for incoming serial data

///////////////////////
//  Helper Functions //
///////////////////////

// DO NOT WORK
//outputFunction = arraySize(pins);
//int arraySize(int thisArray[])
//{
//  return (int) (sizeof(thisArray)/sizeof(int));
//}
// DO NOT WORK

//Do not use 0 and 1 because of the Tx and Rx
//Max pin #13
int verifyPump(int pin)
{
  if (pin>=2 && pin<=13)
  {
    return true;
  }
  return false;
}

int isSetPump(int pin)
{
  for (int i = 0; i < pinCount; i++)
  {
    outputSerial = "get pin ";
    outputSerial += i;
    outputSerial += " = ";
    outputSerial += pins[i];
    Serial.println(outputSerial);
    if (pins[i] == pin)
    {
      return true;
    }
  }
  return false;
}

//min 50
//max 10 000
int verifyDelay(int millisec)
{
  if (millisec>=50 && millisec<=10000)
  {
    return true;
  }
  return false;
}

//must be equal to zero
int checkGlass(int sensorValue)
{
  if (sensorValue == 0)
  {
    return true;
  }
  return false;
}

///////////////////////
//  Config Functions //
///////////////////////

void setup()
{
  Serial.begin(9600); // opens serial port, sets data rate to 9600 bps
  
  //set pins
//  outputFunction = (sizeof(pins)/sizeof(int));
//  outputSerial = "set pins :";
//  outputSerial += outputFunction;
//  Serial.println(outputSerial);
  for (int i = 0; i < pinCount; i++)
  {
    pins[i] = i+2;
    outputSerial = "set pin ";
    outputSerial += i;
    outputSerial += " = ";
    outputSerial += pins[i];
    Serial.println(outputSerial);
  }
  
  //init Pump pins
//  outputFunction = (sizeof(pins)/sizeof(int));
//  outputSerial = "set pins :";
//  outputSerial += outputFunction;
//  Serial.println(outputSerial);
  for (int i = 0; i < pinCount; i++)
  {
    outputSerial = "get pin ";
    outputSerial += i;
    outputSerial += " = ";
    outputSerial += pins[i];
    Serial.println(outputSerial);
    if (verifyPump(pins[i]))
    {
      pinMode(pins[i],OUTPUT); // LED output
      Serial.println("Pump pin is active");
    }
    else
    {
      Serial.println("Pump pin is not valid");
    }
  }
}

/////////////////////
//  Main Functions //
/////////////////////

void loop()
{
  Serial.println("Start Loop");
  
    // read the incoming data:
    outputSerial = "read the incommingInstruction(pumpNo[2-13], TimeInMillisec[50-10000])";
    Serial.println(outputSerial);
    outputSerial = "Ex: 2,3333 Ex2: 3,4444 Ex3: 4,5555";
    Serial.println(outputSerial);
    
    outputFunction = incommingInstruction();
    outputSerial = "incommingInstruction :";
    outputSerial += outputFunction;
    Serial.println(outputSerial);
    
    if(outputFunction)
    {
      Serial.println("incommingInstruction succed");
      
      outputFunction = ActivatePump(instruction[0], instruction[1]);
      outputSerial = "ActivatePump :";
      outputSerial += outputFunction;
      Serial.println(outputSerial);
    }
    else
    {
      Serial.println("ERROR - incommingInstruction fail");
    }
  
  outputSerial = "Colddown :";
  outputSerial += 10;
  Serial.println(outputSerial);
  delay(10); // Colddown
}

int incommingInstruction()
{
    unsigned long starttime;
    starttime = millis();
    
    outputFunction = (sizeof(instruction)/sizeof(int));
    outputSerial = "Count parameters :";
    outputSerial += outputFunction;
    Serial.println(outputSerial);
    while ( (Serial.available()<outputFunction) && ((millis() - starttime) < MAX_MILLIS_TO_WAIT) )
    {      
      // hang in this loop until we either get number of bytes of data or 1 second
      // has gone by
    }
    if(Serial.available() < outputFunction)
    {
      // the data didn't come in - handle that problem here
      Serial.println("ERROR - Didn't get the good number of bytes for data!");
    }
    else
    {
      outputSerial = "incomingData";
      Serial.println(outputSerial);
      for(int n=0; n<outputFunction; n++)
        instruction[n] = Serial.parseInt(); // Then: Get them. 
        
      for (int i = 0; i < outputFunction; i++)
      {
        outputSerial = "get instruction ";
        outputSerial += i;
        outputSerial += " = ";
        outputSerial += instruction[i];
        Serial.println(outputSerial);
      }
      
      //SPECIAL FUNCTION HERE
      
      
      if (checkGlass(getSensorValue(sensorPin)))
      {
        Serial.println("checkGlass succed");
      }
      else
      {
        Serial.println("ERROR - checkGlass fail");
        return false;
      }
      
      if (verifyPump(instruction[0]) && isSetPump(instruction[0]))
      {
        Serial.println("verifyPump and isSetPump succed");
      }
      else
      {
        Serial.println("ERROR - verifyPump or isSetPump fail");
        return false;
      }
      
      if (verifyDelay(instruction[1]))
      {
        Serial.println("verifyDelay succed");
      }
      else
      {
        Serial.println("ERROR - verifyDelay fail");
        return false;
      }
      
      return true;
    }
    return false;
}

int getSensorValue(int pin)
{
  outputFunction = analogRead(pin);
  outputSerial = "getSensorValue pin ";
  outputSerial += pin;
  outputSerial += " = ";
  outputSerial += outputFunction;
  Serial.println(outputSerial);
  return outputFunction;
}


int ActivatePump(int pinPump, int delayPump)
{
  outputFunction = StartPump(pinPump);
  outputSerial = "StartPump ";
  outputSerial += pinPump;
  outputSerial += " :";
  outputSerial += outputFunction;
  Serial.println(outputSerial);
  
  outputSerial = "delay for ";
  outputSerial += delayPump;
  Serial.println(outputSerial);
  delayPump += PUMP_START;
  delay(delayPump);
  
  outputFunction = StopPump(pinPump);
  outputSerial = "StopPump ";
  outputSerial += pinPump;
  outputSerial += " :";
  outputSerial += outputFunction;
  Serial.println(outputSerial);
  
  return true;
}

int StartPump(int pinPump)
{
   digitalWrite(pinPump,HIGH);
   return true;
}

int StopPump(int pinPump)
{
   digitalWrite(pinPump,LOW); 
   return true;
}
