<?php

date_default_timezone_set('America/Montreal');

// Logic
// Arduino -> Pump -> Breuvage 
// fn getTime


class arduino
{
	private $arduinoServer =  '';

	public function __construct($arduinoServer)
	{
		$this->setArduinoServer($arduinoServer);
	}

	public function setArduinoServer($arduinoServer)
	{
		$this->arduinoServer = $arduinoServer;
	}

	public function getArduinoServer()
	{
		return $this->arduinoServer;
	}


	public function arduinoResquest($arduinoServer, $request)
	{
// $curl = curl_init($arduinoServer . $request);
// echo curl_exec($curl);
		return file_get_contents($arduinoServer . $request);;
	}

	public function initInstruc($pumpID, $mode = 'output')
	{
		return 'arduino/mode/'.$pumpID.'/'.$mode ;
	}

	private function pumpInstruc($pumpID, $mode)
	{
		return 'arduino/digital/'.$pumpID.'/'.$mode;
	}

	public function pumpActivate($pumpID)
	{
		return $this->pumpInstruc($pumpID, 1);
	}

	public function pumpDeactivate($pumpID)
	{
		return $this->pumpInstruc($pumpID, 0);
	}
}

$arduinoServer =  'http://192.168.70.105/'; // 'http://arduino.local/'; latency on domaine name resolution
$arduino = new arduino($arduinoServer);

echo 'Init'.'<br />';
echo $arduino->initInstruc(11).'<br />';
echo $arduino->arduinoResquest($arduino->getArduinoServer(), $arduino->initInstruc(11)).'<br />';
echo '###'.'<br />';


echo 'Activate'.'<br />';
echo date('H:i:u').'<br />';
echo $start = microtime().'<br />';
echo $arduino->pumpActivate(11).'<br />';
echo $arduino->arduinoResquest($arduino->getArduinoServer(), $arduino->pumpActivate(11)).'<br />';
echo '###'.'<br />';
echo date('H:i:u').'<br />';
echo $end = microtime().'<br />';
echo 'delay : '. $end - $start.'<br />';

$timeWait = 2000000;
echo 'Wait for '. $timeWait/1000000 .'s '.'<br />';
usleep($timeWait); //in microsec 2000000 = 2 sec

echo 'Deactivate'.'<br />';
echo $arduino->pumpActivate(11).'<br />';
echo $arduino->arduinoResquest($arduino->getArduinoServer(), $arduino->pumpDeactivate(11)).'<br />';
echo '###'.'<br />';

